import json


class Students:

    def __init__(self):
        with open("students.json", encoding='utf-8') as f:
            self.students_data = json.loads(f.read())

    def search_student_by_name(self, search_text):
        for student in self.students_data:
            if search_text in student["Name"]:
                return student["ID"]

    def filter_by_gender(self, search_text):
        stud_list = [student for student in self.students_data
                     if search_text in student["Gender"]]
        print(stud_list)

    def search_by_class_and_club(self, club, cl):
        for student in self.students_data:
            if student["Class"] == cl and student["Club"] == club:
                yield student


student = Students()
print(student.search_student_by_name("Haruki"))
student.filter_by_gender("W")
s = student.search_by_class_and_club(club="Football", cl="5a")
for i in s:
    print(i)
