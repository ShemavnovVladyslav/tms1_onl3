import xml.etree.ElementTree as ET

root = ET.parse("library.xml").getroot()


def lib(value, text):
    for elements in root:
        for element in elements:
            if element.tag == value and text in element.text:
                print(elements.attrib["id"])


lib("author", "Corets")
lib("price", "7")
lib("title", "Midnight Rain")
