import yaml
import json

with open("order.yaml", encoding='utf-8') as f:
    order_data = yaml.safe_load(f.read())


def show_info_from_yaml_file(type_info):
    print(type_info)


def show_discription_price_count():
    for item in order_data["product"]:
        print(f"Description: {item['description']}, "
              f"Price: {item['price']},"
              f" Count: {item['quantity']}")


show_info_from_yaml_file(type_info=order_data["invoice"])
show_info_from_yaml_file(type_info=order_data["bill-to"]["address"])
show_discription_price_count()


with open("yaml_to_json.json", "w") as new_json_file:
    json.dump(str(order_data), new_json_file)

create_yaml_file = {"Ownew": {"First Name": "Vladyslav",
                              "Last Name": "Shemavnov"}}

with open("my_yaml_file.yaml", "w") as my_file:
    yaml.dump(create_yaml_file, my_file)
