import requests
import json

headers = {
    "Content-Length": "142",
    "Content-Type": "text/json; v=1.0",
    "Host": "fakerestapi.azurewebsites.net"
}


# GET
url = "https://fakerestapi.azurewebsites.net/api/v1/Authors"
response = requests.request("GET", url=url)
print(response.json())


author_id = 1
url2 = f"https://fakerestapi.azurewebsites.net/api/v1/Authors/{author_id}"
resp = requests.request("GET", url=url2)
print(resp.json())


# POST

url3 = "https://fakerestapi.azurewebsites.net/api/v1/Books"
data = json.dumps({
    "id": 1000,
    "title": "My book",
    "description": "Story about my hero",
    "pageCount": 300,
    "excerpt": "string",
    "publishDate": "2021-09-27T17:56:21.831Z"
})
resp3 = requests.request("POST", url=url3, data=data, headers=headers)
print(resp3)


url4 = "https://fakerestapi.azurewebsites.net/api/v1/Authors"
data2 = json.dumps({
    "id": 1000,
    "idBook": 1000,
    "firstName": "Vladyslav",
    "lastName": "Shemavnov"
})
resp4 = requests.request("POST", url=url4, data=data2, headers=headers)
print(resp4)


# PUT
book_id = 10
url5 = f"https://fakerestapi.azurewebsites.net/api/v1/Books/{book_id}"
data_book = json.dumps({
    "id": 10,
    "title": "Update book",
    "description": "Update book",
    "pageCount": 300,
    "excerpt": "string",
    "publishDate": "2021-09-27T21:11:23.927Z"
})
resp5 = requests.request("PUT", url=url5, data=data_book, headers=headers)
print(resp5)


# DELETE
user_id = 4
url6 = f"https://fakerestapi.azurewebsites.net/api/v1/Users/{user_id}"
resp6 = requests.request("DELETE", url=url6)
print(resp6)
