from langdetect import detect

list_name_0 = []
list_name_1 = ["Ann"]
list_name_2 = ["Ann", "Alex"]
list_name_3 = ["Ann", "Alex", "Mark"]
list_name_4 = ["Ann", "Alex", "Mark", "Max"]
list_name_1_ru = ["Аня"]
list_name_2_ru = ["Аня", "Леша"]
list_name_3_ru = ["Аня", "Леша", "Максим"]
list_name_4_ru = ["Аня", "Леша", "Максим", "Ваня"]


def like(name_list):
    if detect(name_list[0]) == 'tl':
        if len(name_list) >= 4:
            print(f"{name_list[0]}, {name_list[1]} and "
                  f"{len(name_list[2:])} others like this")
        elif len(name_list) == 3:
            print(f"{name_list[0]}, {name_list[1]} "
                  f"and {name_list[2]} like this")
        elif len(name_list) == 1:
            print(f"{name_list[0]} like this")
        elif len(name_list) == 0:
            print("No one likes this")
        else:
            print(f"{name_list[0]}, {name_list[1]} like this")
    elif detect(name_list[0]) == 'uk':
        if len(name_list) >= 4:
            print(f"{name_list[0]}, {name_list[1]} и "
                  f"{len(name_list[2:])} остальным нравиться это")
        elif len(name_list) == 3:
            print(f"{name_list[0]}, {name_list[1]} "
                  f"и {name_list[2]} нравиться это")
        elif len(name_list) == 1:
            print(f"{name_list[0]} нравиться это")
        elif len(name_list) == 0:
            print("Пока нет реакций")
            # не придумал как проверить пустой списко
        else:
            print(f"{name_list[0]}, {name_list[1]} нравиться это")


like(list_name_4_ru)
