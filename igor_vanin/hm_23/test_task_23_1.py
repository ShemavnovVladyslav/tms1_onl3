from selenium import webdriver
from selenium.webdriver.common.by import By


def test_task_24_1():
    url = 'https://ultimateqa.com/complicated-page/'
    x_path = '//a[@class="et_pb_button et_pb_button_4 ' \
             'et_pb_bg_layout_light"]'
    css = '.et_pb_column_1_4.et_pb_column_3>:nth-child(2)' \
          ' > .et_pb_bg_layout_light'
    cls = '[class="et_pb_button et_pb_button_4 et_pb_bg_layout_light"]'
    browser = webdriver.Chrome()
    browser.implicitly_wait(7)
    browser.get(url)
    browser.find_element(By.XPATH, x_path).click()
    browser.find_element(By.CSS_SELECTOR, css).click()
    browser.find_element(By.CSS_SELECTOR, cls).click()
    browser.quit()
