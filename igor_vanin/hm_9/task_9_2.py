class Investment:
    def __init__(self, fin, term, percent=10):
        self.fin = fin
        self.term = term
        self.percent = percent


class Bank:
    def deposit(self, investm):
        for it in range(investm.term):
            investm.fin += investm.fin * (investm.percent // (12 * 100))
        return print(investm.fin)


contribution = Investment(2000, 2)
bank = Bank()
bank.deposit(contribution)
