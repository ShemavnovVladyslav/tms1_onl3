class Locators:

    def __init__(self):
        self.first_name = '[name="firstName"]'
        self.last_name = 'input[name="lastName"]'
        self.phone = 'input[name="phone"]'
        self.email = 'input[name="userName"]'
        self.address = 'input[name="address1"]'
        self.city = 'input[name="city"]'
        self.state = 'input[name="state"]'
        self.postal_code = 'input[name="postalCode"]'
        self.user_name = 'input[name="email"]'
        self.password = 'input[name="password"]'
        self.confirm_pass = 'input[name="confirmPassword"]'
        self.button = 'input[name="submit"]'
        self.title_last_first_name = '//b[contains(text(), "Dear")]'
        self.user_name_title = '//b[contains(text(), "Note")]'
