from selenium import webdriver
import time


def test_wiki_search():
    url = "https://en.wikipedia.org/"
    path = "C:/chromedriver.exe"
    word = "programming language"
    driver = webdriver.Chrome(path)
    driver.get(url)
    search = driver.find_element_by_xpath('//*[@id="searchInput"]')
    search.send_keys(word)
    search.submit()
    time.sleep(1)
    title = driver.find_element_by_xpath('//*[@id="firstHeading"]').text
    assert word == title, f'{title} is not equal {word}'
