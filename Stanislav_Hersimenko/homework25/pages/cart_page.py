from pages.base_page import BasePage
from locators.cart_page_locators import CartPageLocators


class CartPage(BasePage):

    def should_be_cart_page_title(self):
        cart = self.find_element(CartPageLocators.LOCATOR_CART_TITLE).text
        assert cart == "Your shopping cart"

    def should_be_cart_page_summary(self):
        cart = self.find_element(CartPageLocators.LOCATOR_CART_SUMMARY).text
        assert cart == "SHOPPING-CART SUMMARY"

    def should_be_cart_not_empty(self):
        not_empty_cart = self.find_element(
            CartPageLocators.LOCATOR_CART_NOT_EMPTY).text
        assert not_empty_cart == "Your shopping cart contains: 1 Product"

    def should_be_deleted(self):
        not_empty_cart = self.find_element(
            CartPageLocators.LOCATOR_ITEM_DELETE)
        not_empty_cart.click()

    def should_cart_update(self):
        update_cart = self.find_element(
            CartPageLocators.LOCATOR_CART)
        update_cart.click()

    def should_be_cart_empty(self):
        empty_cart = self.find_element(
            CartPageLocators.LOCATOR_CART_EMPTY).text
        assert empty_cart == "Your shopping cart is empty."
