from Kseniya_Likhtarovich.hm_25.pages.base_page import BasePage
from Kseniya_Likhtarovich.hm_25.locators.login_page_locators \
    import LoginPageLocators


class LoginPage(BasePage):

    def should_be_login_page(self):
        account_text = self.find_element(LoginPageLocators.LOCATOR_ACCOUNT)
        assert account_text.text == "Authentication"

    def login_error(self):
        button = self.find_element(LoginPageLocators.LOCATOR_SUBMIT)
        button.click()

        error_msg = self.find_element(LoginPageLocators.LOCATOR_ERROR)
        assert error_msg.text == "An email address required."

    def login(self, email: str, password: str):
        email_field = self.find_element(LoginPageLocators.LOCATOR_EMAIL)
        email_field.send_keys(email)

        password_field = self.find_element(LoginPageLocators.LOCATOR_PASSWD)
        password_field.send_keys(password)

        button = self.find_element(LoginPageLocators.LOCATOR_SUBMIT)
        button.click()
