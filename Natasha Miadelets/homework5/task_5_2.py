likes = ["Anna", "Vadim", "Natasha", "Dima", "Nina", "Koliya"]
# likes = ["Аня", "Вадим", "Наташа", "Дима", "Нина", "Коля"]
a = likes[0]
x = a.find("н")
b = len(likes)

c = b - 2
if x < 0 and b == 0:
    print("no one likes this")
elif x < 0 and b == 1:
    print(likes[0], "likes this")
elif x < 0 and b == 2:
    print(likes[0], "and", likes[1], "like this")
elif x < 0 and b == 3:
    print(likes[0], ",", likes[1], "and", likes[2], "like this")
elif x < 0:
    print(likes[0], ",", likes[1], "and", f"{c} others like this")

if x > 0 and b == 0:
    print("нет отметок")
elif x > 0 and b == 1:
    print(likes[0], "нравится это")
elif x > 0 and b == 2:
    print(likes[0], "и", likes[1], "нравиться это")
elif x > 0 and b == 3:
    print(likes[0], ",", likes[1], "и", likes[2], "нравится это")
elif x > 0:
    print(likes[0], ",", likes[1], "и", f"{c} другим нравится это")
