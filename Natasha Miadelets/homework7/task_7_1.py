def reshape(list1: list, row: int, count: int):
    list_new = []
    x = 0
    list_new.append(list1[0:count])
    while x < row - 1:
        list_new.append(list1[count * (x + 1):count * (x + 2)])
        x += 1
    print(list_new)


# reshape([1, 2, 3, 4, 5, 6], 2, 3)
reshape([1, 2, 3, 4, 5, 6, 7, 8], 4, 2)
