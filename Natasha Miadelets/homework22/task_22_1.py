from selenium import webdriver
import time

url = "http://thedemosite.co.uk/login.php"
browser = webdriver.Chrome("./chromedriver 2")
username = "natasha.mi"
password = "123456"
browser.get(url)
xpath_username_input = "/html/body/table/tbody/tr/td[1]/" \
                       "form/div/center/table/tbody/tr/td[1]/" \
                       "table/tbody/tr[1]/td[2]/p/input"
xpath_password_input = "/html/body/table/tbody/tr/td[1]/form/" \
                       "div/center/table/tbody/tr/td[1]/table/" \
                       "tbody/tr[2]/td[2]/p/input"
xpath_test_login_button = "/html/body/table/tbody/tr/td[1]/form/" \
                          "div/center/table/tbody/tr/td[1]/table/" \
                          "tbody/tr[3]/td[2]/p/input"
xpath_text = "/html/body/table/tbody/tr/td[1]/big/blockquote/" \
             "blockquote/font/center/b"
username_input = browser.find_element_by_xpath(xpath_username_input)
username_input.send_keys(username)
password_input = browser.find_element_by_xpath(xpath_password_input)
password_input.send_keys(password)
test_login_button = browser.find_element_by_xpath(xpath_test_login_button)
time.sleep(2)
test_login_button.click()
time.sleep(2)
text = browser.find_element_by_xpath(xpath_text).text
assert text == "**Successful Login**",\
    f'**Successful Login** is not equal {text}'
browser.quit()
