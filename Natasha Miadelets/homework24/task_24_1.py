from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException


def check_exists_by_xpath(xpath):
    try:
        browser.find_element_by_xpath(xpath)
    except NoSuchElementException:
        return False
    return True


url = "http://the-internet.herokuapp.com/dynamic_controls"
browser = webdriver.Chrome("./chromedriver 2")
browser.implicitly_wait(5)
browser.get(url)

checkbox_xpath = "//input[@type='checkbox']"
button_xpath = "//button[text()='Remove']"
text1_xpath = "//p[text()=\"It's gone!\"]"
input_dis_xpath = "//input[@type='text' and @disabled]"
enable_but_xpath = "//button[text()='Enable']"
text2_xpath = "//p[text()=\"It's enabled!\"]"
input_en_xpath = "//input[@type='text' and @style]"

checkbox = browser.find_element_by_xpath(checkbox_xpath)
checkbox.click()
button = browser.find_element_by_xpath(button_xpath)
button.click()
text1 = browser.find_element_by_xpath(text1_xpath).text
assert text1 == 'It\'s gone!', f'It\'s gone! is not equal {text1}'
check_exists_by_xpath(checkbox_xpath)

input_dis = browser.find_element_by_xpath(input_dis_xpath)

if input_dis.is_enabled():
    input_dis.click()
else:
    pass

enable_but = browser.find_element_by_xpath(enable_but_xpath)
enable_but.click()

text2 = browser.find_element_by_xpath(text2_xpath).text
assert text2 == "It's enabled!", f"It's enabled! is not equal {text2}"


input_en = browser.find_element_by_xpath(input_en_xpath)
if input_en.is_enabled():
    input_en.click()
else:
    pass

browser.quit()
