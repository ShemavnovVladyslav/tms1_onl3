import yaml
import json

with open("order.yaml", encoding='utf-8') as f:
    order_data = yaml.safe_load(f.read())


def print_number_and_adress():
    print("Номер заказа:", order_data["invoice"])
    print("Адрес отправки:", order_data["bill-to"]["address"])


def print_desc_price_qual():
    for i in order_data["product"]:
        desc, price, qua = i["description"], i["price"], i["quantity"]
        print(f"Описание: {desc}, цена: {price}, кол-во: {qua}")


print_number_and_adress()
print_desc_price_qual()

with open("yaml_for_json.json", "w") as j_file:
    json.dump(str(order_data), j_file)

create_yaml_file = {"User": {"First Name": "Rinat",
                             "Course": "Python Automation"}}

with open("ex_yaml_file.yaml", "w") as file:
    yaml.dump(create_yaml_file, file)
