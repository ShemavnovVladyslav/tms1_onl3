from Rinat_Amirzhanov.homework25.locators.login_page_locators\
    import LoginPageLocators
from Rinat_Amirzhanov.homework25.pages.BasePage import BasePage


class LoginPageHelper(BasePage):

    def should_be_login_page(self):
        account_text = self.find_element(LoginPageLocators.LOCATOR_ACCOUNT)
        assert account_text.text == "Authentication"

    def login(self, email, passwd):
        email_field = self.find_element(LoginPageLocators.EMAIL_FIELD)
        email_field.send_keys(email)
        password = self.find_element(LoginPageLocators.PASSWORD_FIELD)
        password.send_keys(passwd)
        button = self.find_element(LoginPageLocators.SIGN_IN_BUTTON)
        button.click()

    def login_error(self):
        button = self.find_element(LoginPageLocators.SIGN_IN_BUTTON)
        button.click()
