from program import game_bulls
import pytest


@pytest.fixture(autouse=True)
def start_tests():
    print("Tests started")


def test_win():
    assert game_bulls(9743, 9743) == "Вы выйграли!"


def test_bulls_3():
    assert game_bulls(9743, 9742) == [3, 0]


def test_bulls_1():
    assert game_bulls(5129, 5034) == [1, 0]


def test_cows_4():
    assert game_bulls(3456, 6534) == [0, 4]


def test_cows_1():
    assert game_bulls(4369, 1054) == [0, 1]


@pytest.mark.xfail
def test_empty():
    assert game_bulls(4569, "")
