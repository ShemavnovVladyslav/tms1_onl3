import itertools


def text(word):
    for k, v in itertools.groupby(word):
        count_letter = len(list(v))
        result = str(count_letter if count_letter > 1 else "")
        print(k + result, end='')


text("cccbba")
text("abeehhhhhccced")
text("aaabbceedd")
text("abcde")
text("aaabbdefffff")
