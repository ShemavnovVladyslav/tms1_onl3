a = {'a': 1, 'b': 2, 'c': 3}
b = {'c': 3, 'd': 4, 'e': 5}
z = {**a, **b}
print(z)
for k in z.keys():
    if k in a and k in b:
        result = [a[k], b[k]]
        z[k] = result
    elif k in a and k not in b:
        result = [a[k], "None"]
        z[k] = result
    elif k not in a and k in b:
        result = ["None", b[k]]
        z[k] = result
print(z)
