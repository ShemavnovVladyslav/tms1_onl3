from nikita_tyushkevich.homework25.pages.base_page import BasePage
from nikita_tyushkevich.homework25.locators.login_page_locators\
    import LoginPageLocators


class LoginPage(BasePage):
    def should_be_login(self):
        create_account = self.find_element(
            LoginPageLocators.LOCATOR_CREATE_ACCOUNT_TEXT).text
        assert create_account == "CREATE AN ACCOUNT"

    def login(self, email: str, passwd: str):
        email_field = self.find_element(LoginPageLocators.LOCATOR_EMAIL_FIELD)
        email_field.send_keys(email)
        passwd_field = self.find_element(
            LoginPageLocators.LOCATOR_PASSWORD_FIELD)
        passwd_field.send_keys(passwd)
        self.find_element(LoginPageLocators.LOCATOR_SUBMIT_BUTTON).click()
