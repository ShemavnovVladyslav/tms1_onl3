numbers = [34.6, -203.4, 44.9, 68.3, -12.2, 44.6, 12.7]

result_list = []


# creating generator
def new_list(numbers: list):
    for i in range(len(numbers)):
        if numbers[i] > 0:
            result_list.append(numbers[i])
        yield result_list


# Go thru all iterations to print each iteration in result_list
result_generator = new_list(numbers)
for j in result_generator:
    continue

print(result_list)
