import collections
from collections import Counter

a = "Hello World! www BbBb"

# convert to low case
a_lower = a.lower()

# remove all symbols except letters
a_converted = "".join(i for i in a_lower if i.isalnum())

# Count all symbols in string
count = Counter(a_converted)

# Find all most common symbols
mc = dict(count.most_common())

# Find max value of mc dict
max_value = max(mc.values())

# create dict where values = max_value
final_dict = {k: v for k, v in mc.items() if v == max_value}

# sort dict by keys and transform to list
od = list(collections.OrderedDict(sorted(final_dict.items())))
print(f"Answer is: {od[0]}")
