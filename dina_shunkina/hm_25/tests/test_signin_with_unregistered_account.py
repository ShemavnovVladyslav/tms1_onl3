from dina_shunkina.hm_25.pages.main_page import MainPage
from dina_shunkina.hm_25.pages.signin_page import SigninPage


def test_user_singin(browser):
    main_page = MainPage(browser)
    main_page.open_page()
    main_page.should_be_signin_page()
    main_page.open_signin_page()
    signin_page = SigninPage(browser)
    signin_page.should_be_sing_page()
    signin_page.sing_page("D@test.com", "1234567890")
    signin_page.check_error()
